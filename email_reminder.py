#!/usr/bin/python
from datetime import datetime, timedelta
import sqlite3 as sql
import smtplib
from smtp_info import SMTP_SERVER, SMTP_PORT, SENDER, PASSWORD

SUBJECT = "Preenchimento do Protrack (mensagem automatica, nao responda)"
BODY = """
%s %s,

Voce ainda nao preencheu suas informacoes no protrack.

Por favor, preencha-o com suas informacoes da semana antes de 12h de hoje.

A aplicacao encontra-se no endereco http://10.10.27.115/protrack/activities/grid
"""
 
def check_protrack_status(db_path):
    today = datetime.today()
    weekday = today.weekday()
    
    date = today - timedelta(weekday)

    con = sql.connect(db_path)
    cur = con.cursor()

    query = "SELECT first_name, last_name, email FROM auth_user WHERE is_superuser=0 AND is_active=1 AND id NOT IN (SELECT user_id FROM activities_record WHERE date >= '%s')"%date.strftime("%Y-%m-%d")

    cur.execute(query)

    users = []

    for row in cur:
        users.append(row)

    con.close()

    return users

def send_mail_to(user):
    body = BODY%(user[0], user[1])
    headers = ["From: " + SENDER,
               "Subject: " + SUBJECT,
               "To: " + user[2],
               "MIME-Version: 1.0",
               "Content-Type: text/html"]

    headers = "\r\n".join(headers)
 
    recipient = user[2]

    session = smtplib.SMTP(SMTP_SERVER, SMTP_PORT)
    session.ehlo()
    session.starttls()
    session.ehlo
    session.login(SENDER, PASSWORD)
    session.sendmail(SENDER, recipient, headers + "\r\n\r\n" + body)
    session.quit()


if __name__ == "__main__":
    users = check_protrack_status('/var/www/apps/protrack/db.sqlite3')

    if users:
        for user in users:
            send_mail_to(user)

