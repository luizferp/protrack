from jqgrid import JqGrid
from django.core.urlresolvers import reverse_lazy
from activities.models import Record
from datetime import datetime, timedelta

class RecordsGrid(JqGrid):

    time_format = "%H:%M"
    
    fields = ("project__name", "user__first_name", "user__last_name", "id", "activity__name", "date", "comments", "period")

    url = reverse_lazy("grid")

    caption = u'Registro de atividades'
    
    def __init__(self):
        super(RecordsGrid, self).__init__()
        
        today = datetime.today()
        weekday = today.weekday()
        
        startdate = today - timedelta(weekday)
        enddate = today + timedelta(4 - weekday)

        self.queryset = Record.objects.filter(date__range=[startdate, enddate]).values(*self.fields)

    def update_query_set(self, userid, startDate, endDate):
        kwargs = {}

        year, month, day = map(int, startDate.split('-'))
        startDate = datetime(year, month, day)

        year, month, day = map(int, endDate.split('-'))
        endDate = datetime(year, month, day)

        kwargs['date__range'] = [startDate, endDate]

        if userid not in ['', None]:
            kwargs['user'] = int(userid)

        self.queryset = Record.objects.filter(**kwargs).values(*self.fields)
