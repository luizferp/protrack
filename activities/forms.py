from django import forms
from django.contrib.auth.models import User


class UserModelChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        return "%s %s"%(obj.first_name, obj.last_name)


class UserForm(forms.Form):
    userquery = User.objects.filter(is_superuser=False).order_by('username')
    users = UserModelChoiceField(queryset=userquery,
                                 label="User",
                                 empty_label="Everybody")
