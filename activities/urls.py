from django.conf.urls import patterns, url

from activities import views

urlpatterns = patterns('',
    # URLs to the helper functions
    url(r'^build_tag/(?P<model_name>\w+)/$', views.build_tag, name='build_tag'),
    url(r'^build_user_tag/$', views.build_user_tag, name='build_user_tag'),
    
    # URLs to the navigation pages
    url(r'^login$', 'django.contrib.auth.views.login', {'redirect_field_name': 'next', 'extra_context': {'next': 'grid'}}),
    url(r'^logoff$', 'django.contrib.auth.views.logout', {'next_page': 'grid'}, name='logoff'),
    url(r'^$', views.index, name='index'),
    url(r'^grid$', views.grid, name='grid'),
    url(r'^grid_config$', views.grid_config, name='grid_config'),
    url(r'^grid_handle$', views.grid_handle, name='grid_handle'),
    url(r'^export_csv$', views.export_csv, name='export_csv'),
    url(r'^validate_csv$', views.validate_csv, name='validate_csv'),
    url(r'^show_users$', views.show_users, name='show_users'),
    url(r'^send_protrack_to_hp$', views.send_protrack_to_hp, name='send_protrack_to_hp'),

    #URLs to outside access
    url(r'^add_post_data$', views.add_post_data, name='add_post_data'),
)
