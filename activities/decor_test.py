def validate_data(f):
    def validate(*args, **kwargs):
        print args
        print kwargs
        return f(*args, **kwargs)
    return validate

@validate_data
def decorated(a, b):
    print "decorated func"
    
decorated("a", b="b")