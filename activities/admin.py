from django.contrib import admin
from activities.models import Project, Activity, Record

# Register your models here.
admin.site.register(Project)
admin.site.register(Activity)
admin.site.register(Record)
