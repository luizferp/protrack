from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Project(models.Model):
    name = models.CharField(max_length=100)
    
    def __unicode__(self):
        return self.name
    
class Activity(models.Model):
    name = models.CharField(max_length=100)
    
    def __unicode__(self):
        return self.name
        
class WorkPeriod(models.Model):
    name = models.CharField(max_length=200)
    start_date = models.DateField()
    end_date = models.DateField()
    
class Record(models.Model):
    project = models.ForeignKey('Project')
    activity = models.ForeignKey('Activity', null=True, blank=True, default=None)
    user = models.ForeignKey(User)
    date = models.DateField()
    comments = models.TextField()
    period = models.TimeField()
    
    def __unicode__(self):
        return self.comments
