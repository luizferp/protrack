﻿from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseServerError, QueryDict
from datetime import time, datetime, timedelta, date
from grid import RecordsGrid
import models
import smtplib
from email.mime.multipart import MIMEMultipart, MIMEBase
from email.mime.text import MIMEText
from email import encoders
from smtp_info import SMTP_SERVER, SMTP_PORT, SENDER, PASSWORD
from forms import UserForm
import unicodedata


def dict_to_querydict(d):
    """
    Converts a value created by querydict_dict back into a Django QueryDict value.
    """
    q = QueryDict("", mutable=True)
    for k, v in d.iteritems():
        q.setlist(k, v)
    q._mutable = False
    return q

def validate(func):
    def validate_request(*args, **kwargs):
        return func(*args, **kwargs)
    return validate_request


@login_required
def index(request):
    context = {}
    return render(request, 'activities/index.html', context)    


@login_required
def grid(request):
    context = {"userform": UserForm()}
    return render(request, 'activities/activities.html', context)
    
def grid_config(request):
    grid = RecordsGrid()
    try:
        grid.update_query_set(request.GET['user'],
                              request.GET['start_date'],
                              request.GET['end_date'])
    except KeyError:
        pass

    return HttpResponse(grid.get_json(request), content_type="application/json")

@login_required
def grid_handle(request):

    post = request.POST
    operation = post.get('oper')
    
    if operation == 'edit':
        isCopy = (post.get('iscopy')) == 'True'
        if (isCopy):
            return grid_copy(post)
        return grid_edit(post)
    elif operation == 'add':
        return grid_add(post)
    elif operation == 'del':
        return grid_del(post)
        
    return HttpResponse()


def grid_del(data):
    record_ids = data.get('id')
    
    for rid in record_ids.split(','):
        record = models.Record.objects.get(id=rid)

        if record:
            record.delete()
    
    return HttpResponse()

def grid_copy(data):

    src_id = data['srcid']

    try:
        src_rec = models.Record.objects.get(id=src_id)
    except ObjectDoesNotExist:
        src_rec = None
    else:
        rec = models.Record()
        
        rec.project_id = src_rec.project_id
        rec.activity_id = src_rec.activity_id
        rec.user_id = src_rec.user_id
        rec.period = src_rec.period
        rec.comments = src_rec.comments

        try:
            year, month, day = map(int, data['date'].split('-'))
            rec.date = date(year, month, day)
        except KeyError:
            rec.date = src_rec.date
        
        rec.save()

    return HttpResponse()


def grid_add(data):
    project_id = data.get('project__name')
    period = data.get('period')
    comments = data.get('comments')
    date = data.get('date')
    activity_id = data.get('activity__name')
    user_id = data.get('user_id')
    
    comments = comments.replace(";", ".")

    rec = models.Record()
    rec.project_id = project_id
    rec.user_id = user_id
    rec.period = period
    rec.comments = comments
    rec.date = date
    
    rec.activity_id = activity_id
    
    rec.save()
    
    return HttpResponse()


def grid_edit(data):
    record_id = data.get('id')
    user_id = data.get('user_id')
    project_id = data.get('project__name')
    activity_id = data.get('activity__name')
    period = data.get('period')
    comments = data.get('comments')
    date = data.get('date')
    
    comments = comments.replace(";", ".")

    try:
        user = User.objects.get(id=user_id)
    except ObjectDoesNotExist:
        user = None
        
    try:
        project = models.Project.objects.get(id=project_id)
    except ObjectDoesNotExist:
        project = None
        
    try:
        activity = models.Activity.objects.get(id=activity_id)
    except ObjectDoesNotExist:
        activity = None
    
    try:
        record = models.Record.objects.get(id=record_id)
    except ObjectDoesNotExist:
        record = None
        
    if record:
        record.comments = comments
        
        hour, minute = period.split(":")
        period = time(int(hour), int(minute))
        record.period = period
        
        record.date = date
        
        if user:
            record.user = user
            
        if project:
            record.project = project
            
        if activity:
            record.activity = activity

        record.save()

    return HttpResponse()
    
    
def build_tag(request, model_name):
    model = getattr(models, model_name).objects.all().order_by('name')
    options = ["<option value='%s'>%s</option>"%(v.id, v.name) for v in model]
    select_tag = "<select>%s</select>"%("".join(options))
    return HttpResponse(select_tag)

    
def build_user_tag(request):
    user_id = request.user.id
    users = User.objects.filter(is_superuser=False).order_by('username')
    options = ["<option value='%s' %s>%s %s</option>"%(u.id, ('', 'selected')[u.id == user_id], u.first_name, u.last_name) for u in users]
    select_tag = "<select>%s</select>"%("".join(options))
    return HttpResponse(select_tag)

    
def export_csv(request):
    fields = ("project__name", "activity__name", "date", "comments", "period", "user__username")
    kwargs = {}

    today = datetime.today()
    weekday = today.weekday()
    
    report_start = request.POST.get('report_start_date')
    report_end = request.POST.get('report_end_date')
    try:
        startdate = datetime.strptime(report_start, "%Y-%m-%d")
    except:    
        startdate = today - timedelta(weekday)

    try:
        enddate = datetime.strptime(report_end, "%Y-%m-%d")
    except:
        enddate = today + timedelta(4 - weekday)

    user_id = request.POST.get('users')
    
    if user_id:
        kwargs['user'] = int(user_id)

    kwargs['date__range'] = [startdate, enddate]

    records = get_records(startdate, enddate, user_id)

    data = []
    
    error_data = []

    date = startdate
    
    while date <= enddate:
        query = "select *, count(*) from activities_record where date = '%s' group by project_id, activity_id, user_id, date having count(*) > 1"%date.strftime("%Y-%m-%d")
        for rec in models.Record.objects.raw(query):
            error_data.append(rec)
        date += timedelta(1)

    context = {"report_start_date": report_start,
               "report_end_date": report_end,
               "user_id": user_id,
               "csv_data": records,
               "error_data": error_data}

    return render(request, 'activities/csv_data.html', context)

def get_records(startdate, enddate, user_id):
    RAW_SQL = """
SELECT id, project_id, activity_id, user_id, GROUP_CONCAT(comments) AS comments, SUM(period) AS total_period
FROM   activities_record
WHERE  date BETWEEN '{startdate}' AND '{enddate}'
       {user_clause}
GROUP BY user_id, project_id, activity_id
"""

    user_clause = ""
    if user_id:
        user_clause = "AND user_id = %s"%user_id

    records = models.Record.objects.raw(RAW_SQL.format(startdate=startdate.strftime('%Y-%m-%d'),
                                                       enddate=enddate.strftime('%Y-%m-%d'),
                                                       user_clause=user_clause))

    user_period_dict = {}
    project_period_dict = {}
    info = {}
    date = startdate.strftime("%d/%m/%Y")
    for record in records:
        data = {}
        user = record.user
        project = record.project
        activity = record.activity
        activity_period = record.total_period
        comments = record.comments

        username = user.username
        projectname = project.name
        key = (username, projectname)

        user_period = user_period_dict.get(username, 0) + activity_period
        project_period = project_period_dict.get(key, 0) + activity_period

        user_period_dict.update({username: user_period})
        project_period_dict.update({key: project_period})

        data.update({'activity': activity.name})
        data.update({'comments': " ".join(set(comments.split(",")))})
        data.update({'period': activity_period})

        activities = info.get(key, [])
        activities.append(data)
        info.update({key: activities})

    data = []
    for key,activities in info.iteritems():
        user = key[0]
        project = key[1]
        project_time = project_period_dict.get(key)
        user_time = user_period_dict.get(user)

        project_timeshare = round((project_time / user_time) * 100)

        comments = []
        for activity in activities:
            comments.append("%s - %s"%(activity.get('activity'), activity.get('comments')))

        row = u"|".join([date, user, project, str(int(project_timeshare)), " e ".join(comments)])

        # Remove all non-ASCII characters
        data.append(unicodedata.normalize("NFKD", row).encode("ISO8859-1", "ignore"))

    return data

def validate_csv(request):

    fields = ("project__name", "activity__name", "date", "comments", "period", "user__username")

    today = datetime.today()
    weekday = today.weekday()
    
    startdate = today - timedelta(weekday)
    enddate = today + timedelta(4 - weekday)
    
    error_data = []

    date = startdate
    
    while date <= enddate:
        query = "select *, count(*) from activities_record where date = '%s' and user_id = '%d' group by project_id, activity_id, user_id, date having count(*) > 1"%(date.strftime("%Y-%m-%d"), request.user.id)
        for rec in  models.Record.objects.raw(query):
            error_data.append(rec)
        date += timedelta(1)


    context = {"error_data":error_data, "csv_data":[]}
    return render(request, 'activities/csv_data.html', context)

def show_users(request):
    today = datetime.today()
    weekday = today.weekday()

    try:
        report_start = request.POST.get('report_start_date')
        startdate = datetime.strptime(report_start, "%Y-%m-%d")
    except:
        startdate = today - timedelta(weekday)

    try:
        report_end = request.POST.get('report_end_date')
        enddate = datetime.strptime(report_end, "%Y-%m-%d")
    except:
        enddate = today + timedelta(4 - weekday)

    missing_users = []
    query = "select id, first_name, last_name from auth_user where is_superuser=0 and is_active=1 and id not in (select distinct user_id from activities_record where date between '%s' and '%s' )"%(startdate.strftime("%Y-%m-%d"), enddate.strftime('%Y-%m-%d'))

    for rec in User.objects.raw(query):
        missing_users.append(rec)

    context = {"csv_data":missing_users}
    return render(request, 'activities/missing_users.html', context)

def add_post_data(request):
    if request.method == 'POST':
        # the client is responsible for sending the data
        # correctly, getting a ServerError otherwise
        #
        # there is no authentication here
        # since this is not an exposed functionality
        
# and there is no sensible information
        #
        # once it's working we can require the client
        # to trade a token with the request to authenticate
        # the user

        try:
            data = request.POST
            mutable = data._mutable
            data._mutable = True
            data['project__name'] = models.Project.objects.get(name=data['project__name']).id
            data['activity__name'] = models.Activity.objects.get(name=data['activity__name']).id
            data['user_id'] = models.User.objects.get(username=data['user_id']).id
            data._mutable = mutable
            response = grid_add(data)
            return response
        except Exception as e:
            return HttpResponseServerError("ERROR %s %s" % (str(e), data))
    return HttpResponseServerError("Get not allowed")

@login_required
def send_protrack_to_hp(request):
    SUBJECT = u"Arquivo de alocação de horas - %s a %s"
    FILENAME = u"Alocacao - %s a %s.txt"
    BODY = u"""Olá, Claudia
 
Segue o arquivo de alocação de horas consolidado desta semana.

Qualquer problema, por favor, me avise.
 
Obrigado,

"""

    fields = ("project__name", "activity__name", "date", "comments", "period", "user__username")

    today = datetime.today()
    weekday = today.weekday()

    try:
        report_start = request.GET.get('report_start_date')
        startdate = datetime.strptime(report_start, "%Y-%m-%d")
    except:    
        startdate = today - timedelta(weekday)

    try:
        report_end = request.GET.get('report_end_date')
        enddate = datetime.strptime(report_end, "%Y-%m-%d")
    except:
        enddate = today + timedelta(4 - weekday)

    fmtstart = startdate.strftime("%d-%m-%Y")
    fmtend = enddate.strftime("%d-%m-%Y")

    user_id = request.GET.get('user_id')
   
    # CSV data
    records = get_records(startdate, enddate, user_id)

    admin = User.objects.get(username='admin')

    data = []
    for r in records:
        data.append(r)

    part = MIMEBase('application', "octet-stream")
    part.set_payload("\r\n".join(data))
    encoders.encode_base64(part)

    filename = FILENAME % (fmtstart, fmtend)
    part.add_header('Content-Disposition', 'attachment; filename="%s"'%filename)

    msg = MIMEMultipart()
    msg['Subject'] = SUBJECT % (fmtstart, fmtend)
    msg['From'] = SENDER
    msg['To'] = admin.email

    msg.attach(part)
    msg.attach(MIMEText(BODY.encode("UTF-8"), 'plain', "UTF-8"))

    session = smtplib.SMTP(SMTP_SERVER, SMTP_PORT)
    session.ehlo()
    session.starttls()
    session.ehlo
    session.login(SENDER, PASSWORD)
    session.sendmail(SENDER, admin.email, msg.as_string())
    session.quit()

    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
 
